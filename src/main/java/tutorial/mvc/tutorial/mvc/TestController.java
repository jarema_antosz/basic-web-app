package tutorial.mvc.tutorial.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Jarek on 2015-12-02.
 */

@Controller
public class TestController {

    @RequestMapping("/test")
    public String test(){

        return "view";
    }
}
